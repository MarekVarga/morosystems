import React, { Component } from 'react';
import './App.css';
import Table from './Table';
import Insert from './Insert';
import PropTypes from 'prop-types';

// App Component
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: []
    };

    this.removeUser = this.removeUser.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.confirmChange = this.confirmChange.bind(this);
    this.myRerender = this.myRerender.bind(this);
    this.getUser = this.getUser.bind(this);
  }

  // set default
  async componentDidMount() {
    const response = await fetch('/api/users');
    const body = await response.json();
    body.sort(function(a, b){
      return a.id - b.id;
    });
    this.setState({users: body});
  }

 	// remove user from array
  async removeUser(id) {
    try {
      await fetch('http://localhost:8080/api/user/'+id, {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      }).then(() => {
        let updatedUsers = this.state.users.filter(i => i.id !== id);
        this.setState({users: updatedUsers});
      }); 
    } catch (e) {

    }

  }

	// update users array after new user has been submitted
	async handleSubmit(user) {
    console.log(user.name, user.username, user.password);
    try {
      await fetch('http://localhost:8080/api/user', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({name: user.name, username: user.username, password: user.password}),
      }).then(() => {
        this.myRerender();
        });
    } catch (e) {

    }
	};

	// update users array after user has been changed
	handleChange(event) {
		const array = [...this.state.users];
		array[event.target.id] = {...array[event.target.id], name: event.target.value};
		this.setState({users: array});
	};

  // confirm change to user
  async confirmChange(id) {
    let result = this.getUser(id);
    try {
      await fetch('http://localhost:8080/api/user/'+id, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(result.name),
      }).then(() => {
        this.myRerender();
      });
    } catch (e) {

    }
  };

  // find user in users
  getUser(id) {
    return this.state.users.filter(i => i.id === id)[0];
  };

  // get all users - rerender
  async myRerender() {
    const response = await fetch('/api/users');
    const body = await response.json();
    body.sort(function(a, b){
      return a.id - b.id;
    });
    this.setState({users: body});
  }

	// render
  render() {
  	const { users } = this.state;

   	return (
   		<div className="container">
   			<Table 
   				userData={users} 
   				removeUser={this.removeUser}
   				changeUser={this.handleChange}
          confirmChange={this.confirmChange}
 	  	/>
 			<Insert handleSubmit={this.handleSubmit} />
    	</div>
    );
  }
}

// validate prop types
//App.propTypes = {
	// nothing to do here?
//}

export default App;
