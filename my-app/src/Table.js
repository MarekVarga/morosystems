import './Table.css';
import TableHeader from './TableHeader';
import TableBody from './TableBody';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

// table Component
class Table extends Component {
	// render
	render() {
		const { userData, removeUser, changeUser, confirmChange } = this.props;

		return (
			<table>
				<TableHeader />
				<TableBody 
					userData={userData} 
					removeUser={removeUser}
					changeUser={changeUser}
					confirmChange={confirmChange}
				/>
     		</table> 
		);
	}
}

// validate prop types
Table.propTypes = {
	userData: PropTypes.array.isRequired,
	removeUser: PropTypes.func.isRequired,
	changeUser: PropTypes.func.isRequired,
}

export default Table;
