import './Insert.css'
import PropTypes from 'prop-types';
import React, { Component } from 'react';

// insert Component
class Insert extends Component {

	// constructor
	constructor(props) {
		super(props);

		this.initialState = {
			id: 3,
			name: '',
			username: '',
			password: ''
		};

		this.state = this.initialState;
	}

	// update user array
	handleChange = event => {
		const {name, value} = event.target;

		this.setState({
			[name]: value
		});
	}

	/*
	handleUsernameChange = event => {
		const {name, value} = event.target;
	}
	*/

	// handle new user addition
	submitInsert = (event) => {
		event.preventDefault();
		
		this.props.handleSubmit(this.state);
		this.setState(this.initialState);
		this.incrementID();
	}

	// increment user ID after new user has been added
	incrementID = () => {
		this.setState({ id: this.state.id + 1});
	}

	// render
	render() {
		const {id, name, username, password} = this.state;

		return (
			<form onSubmit={this.submitInsert}>
				<div>
				<label className="label">User name:</label>
				<input
					className="Name"
					type="text"
					name="name"
					value={name}
					onChange={this.handleChange} />
				</div>
				<div>
				<label className="label">User username:</label>
				<input
					className="Username"
					type="text"
					name="username"
					value={username}
					onChange={this.handleChange} />
				</div>
				<div>
				<label className="label">User password:</label>
				<input
					className="Password"
					type="password"
					name="password"
					value={password}
					onChange={this.handleChange} />
				</div>
				<button className="submit" type="submit">ADD USER</button>
			</form>
		);
	}
}

// validate prop types
Insert.propTypes = {
	handleSubmit: PropTypes.func.isRequired,
}

export default Insert;