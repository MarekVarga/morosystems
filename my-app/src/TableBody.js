import TableData from './TableData';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// table body Component
class TableBody extends Component {

	// constructor
	constructor(props) {
		super(props);

		this.state = {
			users: [],
			text: 'CHANGE'
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleDelete = this.handleDelete.bind(this);
	}

	// change disability of textfield containing user name
	changeDisabilityAndUpdate(index, row) {
		document.getElementById(index).disabled = !document.getElementById(index).disabled;
		document.getElementById(index).value = row.name;
	}

	// change 'CHANGE' button name
	onClickChangeButton(index, id) {
		if (document.getElementById(index*index*100+79).innerHTML === 'CHANGE') {
			document.getElementById(index*index*100+79).innerHTML = 'DONE';
		} else {
			document.getElementById(index*index*100+79).innerHTML = 'CHANGE';
			this.props.confirmChange(id);
		}
	}

	// handle click on 'CHANGE' button
	handleChange(index, row) {
		this.changeDisabilityAndUpdate(index, row);
		this.onClickChangeButton(index, row.id);
	}

	// set default
	componentDidMount() {
		fetch('/api/users')
			.then(response => response.json())
			.then(data => this.setState({users: data}));
		//this.setState({users:this.props.userData});
	}

	// handle click on 'DELETE' button
	handleDelete(index) {
		this.props.removeUser(index);
	}

	// render
	render () {
		const { userData, removeUser, changeUser, confirmChange} = this.props;
		const rows = this.props.userData.map((row, index) => {
			return (
				<tr key={index}>
					<TableData className="rowId" userInfo1={row.id}/>
					<TableData userInfo1={<input type="text" id={index} name="Name" value={row.name} disabled={true} onChange={this.props.changeUser}/>} />
					<TableData className="modify" 
						userInfo1={<button className="delete" onClick={() => {this.handleDelete(row.id)}}>DELETE</button>} 
						userInfo2={<button id={index*index*100+79} className="change" onClick={() => {this.handleChange(index, row)}}>CHANGE</button>} 
					/>
				</tr>
			);
		});

		return <tbody>{rows}</tbody>;
	}
}

// validate prop types
TableBody.propTypes = {
	userData: PropTypes.array.isRequired,
	removeUser: PropTypes.func.isRequired,
	changeUser: PropTypes.func.isRequired,
}

export default TableBody;