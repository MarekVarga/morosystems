import React, { Component } from 'react';

// table header Component
class TableHeader extends Component {
	// render
	render () {
		return (
			<thead>
     			<tr>
     				<th>ID</th>
     				<th>NAME</th>
     				<th>MODIFY</th>
     			</tr>
 			</thead>
		);
	}
}

export default TableHeader;