import React, { Component } from 'react';
import PropTypes from 'prop-types';

// table data Component
class TableData extends Component {
	// render
	render () {
		const { userInfo1, userInfo2 } = this.props;

		return (
			<td>{this.props.userInfo1}{this.props.userInfo2}</td>
		);
	}
}

// validate prop types
TableData.propTypes = {
	userInfo1: PropTypes.element.isRequired,
	userInfo2: PropTypes.element,
}

export default TableData;