package com.bean;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

@Entity
@Table(name = "users")
public class User {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String username;
    private String password;

    public User() {
    }

    public User(Long id, String name) {
        this.id = id;
        this.name = name;
        this.username = name;
        this.password = getBase64Enc("");
    }

    public User(String name) {
    	this.name = name;
        this.username = name;
        this.password = getBase64Enc("");
    }

    public User(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = getBase64Enc(password);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getDecodedPassword() {
        return getBase64Dec(this.password);
    }

    public void setPassword(String password) {
        this.password = getBase64Enc(password);
    }

    /**
    *	Method compares whether given object equals to the User
	*
	*	@param obj Object is compared object with this
	*
	*	@return boolean true value is returned when objects are equal otherwise false is returned
    */
	@Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        return Objects.equals(this.id, other.id);
    }
    
    /**
    *	Method returns User's id and name in String format
    *
    *	@return String User's id and name
    */
    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", username=" + username + ", password=" + password + "}";
    }

    /**
    *   Method converts given string to Base64 encoding
    */
    public String getBase64Enc(String rawPassword) {
        String returned = "";

        try {
            returned = Base64.getEncoder().encodeToString(rawPassword.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            System.out.println("Error: " + e.getMessage());
        }

        return returned;
    }

    /**
    *   Method converts encoded password in Base64 encoding to string
    *
    *   @param encodedPassword
    *
    *   @return String representation of encodedPassword
    */
    public String getBase64Dec(String encodedPassword) {
        String returned = "";

        byte[] decodedBytes = Base64.getDecoder().decode(encodedPassword);
        returned = new String(decodedBytes);

        return returned;
    }
}
