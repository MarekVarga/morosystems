// Inspired by: https://github.com/hellokoding/hellokoding-series/blob/master/springboot-examples/security/registration-login/src/main/java/com/hellokoding/auth/validator/UserValidator.java
package com.validator;

import com.bean.User;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
	// constants
	private final int usernameMinLength = 3;
	private final int usernameMaxLength = 42;
	private final int passwordMinLength = 4;
	private final int passwordMaxLength = 32;

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        if (user.getUsername().length() < usernameMinLength || user.getUsername().length() > usernameMaxLength) {
            errors.rejectValue("username", "Size.userForm.username");
        }
        if (userService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < passwordMinLength || user.getPassword().length() > passwordMaxLength) {
            errors.rejectValue("password", "Size.userForm.password");
        }
    }
}