package com.service;

public interface SecurityServiceInterface {
	String findLoggedInUsername();

	void autoLogin(String username, String password);
}