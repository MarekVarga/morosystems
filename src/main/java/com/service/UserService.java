package com.service;

import com.bean.User;
import com.repository.UserRepository;
import com.validator.UserValidator;
import com.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

@Service
public class UserService implements UserInterfaceService {
    
    @Autowired
    private UserRepository repository;

    @Override
    public List<User> findAll() {
        List<User> users = (List<User>) repository.findAll();
        
        return users;
    }

    @Override
    public User getUserById(Long id) {
    	return repository.getUserById(id);
    }

    @Override
    public void deleteUserById(Long id) {
        repository.deleteUserById(id);
    }

    @Override
    public User findByUsername(String username) {
        return repository.findByUsername(username);
    }

    @Override
    public ResponseEntity<?> getUser(Long id) {
        // find user
        User user = this.getUserById(id);
        
        if (user == null) {
            // user was not found
            return ResponseEntity.notFound().build();
        } else {
            // user was found
            return ResponseEntity.ok().body(user);
        }
    }

    @Override
    public ResponseEntity<?> updateUser(Long id, User user) {
        // find user
        User foundUser = this.getUserById(id);

        if (foundUser == null) {
            // user was not found
            return ResponseEntity.notFound().build();
        } else {
            // user was found
            foundUser.setName(user.getName());    
            this.repository.save(foundUser);

            return ResponseEntity.ok().body(foundUser);
        }
    }

    @Override
    public ResponseEntity<?> deleteUser(Long id) {
        User deletedUser = this.getUserById(id);

        if (deletedUser == null) {
            return ResponseEntity.notFound().build();
        } else {
            this.deleteUserById(id);

            return ResponseEntity.ok().build();
        }
    }

    @Override
    public ResponseEntity<?> createUser(User user, BindingResult result, SecurityService securityService, UserValidator userValidator) {
        // validate new user
        userValidator.validate(user, result);
        // check for errors
        if (result.hasErrors()) {
            // not a valid user
            return new ResponseEntity<>(result.getAllErrors(), HttpStatus.NOT_ACCEPTABLE);
        } else {
            // everything is valid, new user can be created
            this.repository.save(user);    
            securityService.autoLogin(user.getUsername(), user.getDecodedPassword());

            return ResponseEntity.ok().body(user);
        }
    }
}
