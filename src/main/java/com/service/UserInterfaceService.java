package com.service;

import com.bean.User;
import com.validator.UserValidator;
import com.service.UserService;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

public interface UserInterfaceService {

	/**
    *	Method finds all users and returns them in a List
	*
	*	@return List<User> list of users found in database
    */
    public List<User> findAll();

    /**
    *	Method finds user based on given id
    *
    *	@param id Long id of a user
    *
    *	@return User
    */
    public User getUserById(Long id);

	/**
    *   Method deletes user based on given id
    *
    *   @param id Lond id of user
    */
    public void deleteUserById(Long id);

    /**
    *   Method finds user based on given username
    *
    *   @param username String representation of user's username
    *
    *   @return User
    */
    public User findByUsername(String username);

	/**
	*	Method finds a user based on id given in url
    *
    *	@param id Long is user's id in url
    *
    *	@return ResponseEntity<?> response user -- 200 or not found -- 404 when user was not found
	*/
	public ResponseEntity<?> getUser(Long id);
    
    /**
    *	Method updates user
    *
    *	@param id Long id of user
    *	@param user User is updated user
    *
    *	@return ResponseEntity<?> updated user -- 200 or not found -- 400 when user was not found
    */
    public ResponseEntity<?> updateUser(Long id, User user);

    /**
    *	Method deletes a user based on id given in url
    *
    *	@param id Long is user's id in url
    *
    *	@return ResponseEntity<?> response ok that user was deleted -- 200
    */
    public ResponseEntity<?> deleteUser(Long id);

    /**
    *	Method adds a new user to the database
    *	
    *	@param user User is a new user
    *
    * 	@return ResponseEntiry<?> created user -- 200 or error http status if user's details are invalid
    */
    public ResponseEntity<?> createUser(User user, BindingResult result, SecurityService securityService, UserValidator userValidator);
}
