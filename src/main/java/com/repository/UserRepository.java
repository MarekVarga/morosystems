package com.repository;

import com.bean.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findById(Long id);

	User findByUsername(String username);
	
	User findByName(String name);

	@Transactional
	void deleteUserById(Long id);

	User getUserById(Long id);
}
