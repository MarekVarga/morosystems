package com.controller;

import com.bean.User;
import com.service.UserService;
import com.repository.UserRepository;
import com.service.SecurityService;
import com.validator.UserValidator;
import java.net.URISyntaxException;
import java.util.Collection;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import javax.validation.*;
import org.springframework.validation.BindingResult;

@RestController
@RequestMapping("/api")
public class RequestController {
    
    @Autowired
    UserService userService;

    @Autowired
    SecurityService securityService;

    @Autowired
    UserValidator userValidator;

    @Autowired
    UserRepository repository;

    /**
    *	Method finds all users in database
    *
    *	@return Collection<User> collection of all users
    */
    @GetMapping("/users")
    Collection<User> getAllUsers() {
    	return userService.findAll();
    }

    /**
    *	Method finds a user based on id given in url
    *
    *	@param id Long is user's id in url
    *
    *	@return ResponseEntity<?> response user -- 200 or not found -- 404 when user was not found
    */
    @GetMapping("/user/{id}")
    ResponseEntity<?> getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    /**
    *	Method updates a user
    *
    *   @param id Long is user's id in url
    *	@param user User is updated user
    *
    *	@return ResponseEntity<?> updated user -- 200 or not found -- 400 when user was not found
    */
    @CrossOrigin 
    @PutMapping("/user/{id}")
    ResponseEntity<?> updateUser(@PathVariable Long id, @Valid @RequestBody User user) {
        return userService.updateUser(id, user);
    }

    /**
    *	Method deletes a user based on id given in url
    *
    *	@param id Long is user's id in url
    *
    *	@return ResponseEntity<?> response ok that user was deleted -- 200
    */
    @CrossOrigin 
    @DeleteMapping("/user/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) {
        return userService.deleteUser(id);
    }

    /**
    *	Method adds a new user to the database
    *	
    *	@param user User is a new user
    *
    * 	@return ResponseEntiry<?> created user -- 200 or error http status if user's details are invalid
    */
    @CrossOrigin 
    @PostMapping("/user")
    ResponseEntity<?> createUser(@Valid @RequestBody User user, BindingResult result) throws URISyntaxException {        
        return userService.createUser(user, result, securityService, userValidator);
    }
}
