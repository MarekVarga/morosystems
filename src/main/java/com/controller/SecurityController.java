package com.controller;

import com.service.UserInterfaceService;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;

@RequestMapping("/api/secured")
@RestController
public class SecurityController {

	@Autowired
    UserInterfaceService userService;

	public SecurityController() {
	}

	/**
    *	Method deletes a user based on id given in url
    *
    *	@param id Long is user's id in url
    *
    *	@return ResponseEntity<?> response that user was deleted
    */
    @CrossOrigin 
    @DeleteMapping("/deleteUser/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) {
    	userService.deleteUserById(id);

    	return ResponseEntity.ok().build();
    }
}