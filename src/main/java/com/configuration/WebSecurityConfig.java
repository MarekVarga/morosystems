package com.configuration;

import com.bean.User;
import com.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Base64;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import java.io.UnsupportedEncodingException;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private final UserRepository repository;
    private AuthenticationManagerBuilder auth;

    @Autowired
    private UserDetailsService userDetailsService;

    public WebSecurityConfig(UserRepository repository) {
    	this.repository = repository;
    }

    public void setAuthenticationManagerBuilder(AuthenticationManagerBuilder auth) {
    	this.auth = auth;
    }

    /**
    *	Method specifies permitted and user login required endpoints
    *	
    *	@param http HttpSecurity
    */
    @Override
    protected void configure(HttpSecurity http) throws Exception {    
        http
			.csrf().disable()
			.authorizeRequests()
				.antMatchers("/api/user/*", "/api/users", "/api/user").permitAll()
				.anyRequest().authenticated()
				.and()
			.formLogin();
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    /**
    *	Method configures users' authrorization
    *
    *	@param auth
    */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	/**
	*	Method encodes password with Base64
	*
	*	@return PasswordEncoder
	*/
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new PasswordEncoder() {
			@Override
			public String encode(CharSequence rawPassword) {
				return getBase64Enc(rawPassword.toString());
			}

			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {
				return encodedPassword.equals(getBase64Enc(rawPassword.toString()));
			}
		};
	} 

	/**
	*	Method converts given string to Base64 encoding
	*/
	public String getBase64Enc(String rawPassword) {
		String returned = "";

		try {
			returned = Base64.getEncoder().encodeToString(rawPassword.getBytes("utf-8"));
		} catch (UnsupportedEncodingException e) {
			System.out.println("Error: "+e.getMessage());
		}

		return returned;
	}
}